package com.example.luke.assignment1.controller;

import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.luke.assignment1.R;
import com.example.luke.assignment1.view.AddGeoLocActivity;
import com.example.luke.assignment1.view.MainActivity;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;

/**
 * Created by Luke on 22/09/2016.
 */
public class AddGeoLocActivityController implements View.OnClickListener {

    private AddGeoLocActivity addGeoLocActivity;
    private EditText mapSearch;
    private Intent sentIntent;
    private GoogleMap mMap;
    private Marker marker;

    public AddGeoLocActivityController(AddGeoLocActivity addGeoLocActivity,
                                       EditText mapSearch){
        this.addGeoLocActivity = addGeoLocActivity;
        this.mapSearch = mapSearch;
//        this.sentIntent = sentIntent;
    }

    public void setMap(GoogleMap mMap) {
        this.mMap = mMap;
    }

    @Override
    public void onClick(View view) {

        if(view == view.findViewById(R.id.mapSearchButton)) {
            String searchString = mapSearch.getText().toString();
            Geocoder geocoder = new Geocoder(addGeoLocActivity);
            try {
                List<Address> list = geocoder.getFromLocationName(searchString,1);
                if (list.size() > 0) {
                    Address add = list.get(0);
                    String locality = add.getLocality();
                    Toast.makeText(addGeoLocActivity, "Found: " + locality, Toast.LENGTH_SHORT).show();
                    double lat = add.getLatitude();
                    double lng = add.getLongitude();
                    goTo(lat,lng, 12);
                    if(marker != null){
                        marker.remove();
                    }
                    MarkerOptions options = new MarkerOptions().title(locality).position(new LatLng(lat,lng));
                    marker = mMap.addMarker(options);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        } else if (view == view.findViewById(R.id.mapDoneButton)) {

            if(mapSearch.getText().toString().equals("")){
                Toast.makeText(addGeoLocActivity, "Must Enter Location to Proceed", Toast.LENGTH_SHORT).show();
            } else {
                String searchString = mapSearch.getText().toString();
                Geocoder geocoder = new Geocoder(addGeoLocActivity);
                try {
                    List<Address> list = geocoder.getFromLocationName(searchString,1);
                    if (list.size() > 0) {
                        Address add = list.get(0);
                        double lat = add.getLatitude();
                        double lng = add.getLongitude();
                        String latlng = String.valueOf(lat + ", " + lng);
                        Intent returnData = new Intent();
                        returnData.putExtra(MainActivity.RETURN_EVENT,latlng);
                        addGeoLocActivity.setResult(MainActivity.RESULT_OK, returnData);
                        addGeoLocActivity.finish();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }
    }

    private void goTo(double lat, double lng, float zoom) {
        LatLng location = new LatLng(lat,lng);
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(location, zoom);
        mMap.moveCamera(cameraUpdate);
    }

}
