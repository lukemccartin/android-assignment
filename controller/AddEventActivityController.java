package com.example.luke.assignment1.controller;

import android.content.Intent;
import android.database.Cursor;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import com.example.luke.assignment1.R;
import com.example.luke.assignment1.model.Event;
import com.example.luke.assignment1.model.EventDBOpenHelper;
import com.example.luke.assignment1.model.EventDataSource;
import com.example.luke.assignment1.model.Singleton;
import com.example.luke.assignment1.view.AddContactActivity;
import com.example.luke.assignment1.view.AddEventActivity;
import com.example.luke.assignment1.view.AddGeoLocActivity;
import com.example.luke.assignment1.view.MainActivity;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Luke on 18/08/2016.
 */
public class AddEventActivityController implements View.OnClickListener {

    public static final int DETAIL_REQUEST_2 = 2;
    public static final int DETAIL_REQUEST_3 = 3;
    public static final String PASSED_ARRAY = "PASSED_ARRAY";
    public static final String PASSED_GEOLOC = "PASSED_GEOLOC";
    private ArrayList<String> selectedContactsArray;
    private ArrayList<Event> passedInEventsArray;
    private Intent sentIntent;
    private AddEventActivity addEventActivity;
    private SimpleDateFormat sdfCombine = new SimpleDateFormat("dd/MM/yyyy HH:mm");
    private final Pattern DATE_REGEX = Pattern.compile("([0-9]{2})/([0-9]{2})/([0-9]{4})");
    private final Pattern TIME_REGEX = Pattern.compile("(([0-1][0-9])|([2][0-3])):([0-5][0-9])");
    private EditText title;
    private EditText startDate;
    private EditText startTime;
    private EditText endDate;
    private EditText endTime;
    private EditText venue;
    private EditText notes;
    private EditText geoLoc;

    public AddEventActivityController(AddEventActivity addEventActivity, Intent sentIntent,
                                      ArrayList<String> selectedContactsArray,
                                      ArrayList<Event> passedInEventsArray, EditText title,
                                      EditText startDate, EditText startTime, EditText endDate,
                                      EditText endTime, EditText venue, EditText notes, EditText geoLoc){

        this.selectedContactsArray = selectedContactsArray;
        this.sentIntent = sentIntent;
        this.addEventActivity = addEventActivity;
        this.passedInEventsArray = passedInEventsArray;
        this.title = title;
        this.startDate = startDate;
        this.startTime = startTime;
        this.endDate = endDate;
        this.endTime = endTime;
        this.venue = venue;
        this.notes = notes;
        this.geoLoc = geoLoc;
    }

    @Override
    public void onClick(View view) {


        if(view == view.findViewById(R.id.doneButton)){

            Intent returnData = new Intent();

            if(!(checkInput())) {
                return;
            }

            if(sentIntent.hasExtra(MainActivity.PASSED_EVENT))    {
                Event passedEvent = sentIntent.getParcelableExtra(MainActivity.PASSED_EVENT);
                Event createdEvent = createEventFromInput(passedEvent.getEventId());
                returnData.putExtra(MainActivity.RETURN_EVENT,createdEvent);
            } else {
                Event createdEvent = createEventFromInput();

                for (int i = 0; i < passedInEventsArray.size(); i++) {
                    if(passedInEventsArray.get(i).getEventId().equals(createdEvent.getEventId())){
                        createdEvent = createEventFromInput();
                        i--;
                    }
                }
                returnData.putExtra(MainActivity.RETURN_EVENT,createdEvent);
            }
            addEventActivity.setResult(MainActivity.RESULT_OK, returnData);
            addEventActivity.finish();

        } else if(view == view.findViewById(R.id.addAttendeesButton)) {
            Intent contactIntent = new Intent(addEventActivity, AddContactActivity.class);
            contactIntent.putStringArrayListExtra(PASSED_ARRAY, selectedContactsArray);
            addEventActivity.startActivityForResult(contactIntent, DETAIL_REQUEST_2);
        } else if ((view == view.findViewById(R.id.mapButton))){
            Intent mapIntent = new Intent(addEventActivity, AddGeoLocActivity.class);
            if(!(geoLoc.getText().toString().equals(null) || geoLoc.getText().toString().equals(""))){
                String passedGeoLoc = geoLoc.getText().toString();
                mapIntent.putExtra(PASSED_GEOLOC, passedGeoLoc);
            }
            addEventActivity.startActivityForResult(mapIntent, DETAIL_REQUEST_3);
        }
    }

    public void handleActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == DETAIL_REQUEST_2) {
            if(resultCode == MainActivity.RESULT_OK) {
                ArrayList<String> returnedContactsArray = data.getExtras().getStringArrayList(MainActivity.RETURN_EVENT);
                selectedContactsArray.removeAll(selectedContactsArray);
                for (int j = 0; j < returnedContactsArray.size(); j++) {
                    selectedContactsArray.add(returnedContactsArray.get(j));
                }
            }
        } else if (requestCode == DETAIL_REQUEST_3) {
            if(resultCode == MainActivity.RESULT_OK) {
                String newGeoLoc = data.getExtras().getString(MainActivity.RETURN_EVENT);
                geoLoc.setText(newGeoLoc);
            }
        }
    }

    private boolean checkInput(){

        String inputTest;
        inputTest = title.getText().toString();
        if(inputTest.isEmpty()) {
            Toast.makeText(addEventActivity.getApplicationContext(),
                    "Title cannot be empty.", Toast.LENGTH_SHORT).show();
            return false;
        }

        Matcher match;
        match = DATE_REGEX.matcher(startDate.getText());
        if(!(match.matches())){
            Toast.makeText(addEventActivity.getApplicationContext(),
                    "Incorrect format: Start Date. (dd/mm/yyyy - required).", Toast.LENGTH_SHORT).show();
            return false;
        }

        match = TIME_REGEX.matcher(startTime.getText());
        if(!(match.matches())){
            Toast.makeText(addEventActivity.getApplicationContext(),
                    "Incorrect format: Start Time. (00:00 (24hr format)- required).", Toast.LENGTH_SHORT).show();
            return false;
        }

        match = DATE_REGEX.matcher(endDate.getText());
        if(!(match.matches())){
            Toast.makeText(addEventActivity.getApplicationContext(),
                    "Incorrect format: End Date. (dd/mm/yyyy - required).", Toast.LENGTH_SHORT).show();
            return false;
        }

        match = TIME_REGEX.matcher(endTime.getText());
        if(!(match.matches())){
            Toast.makeText(addEventActivity.getApplicationContext(),
                    "Incorrect format: End Time. (00:00 (24hr format)- required).", Toast.LENGTH_SHORT).show();
            return false;
        }

        String startCompare = startDate.getText().toString();
        String endCompare = endDate.getText().toString();
        startCompare += " " + startTime.getText().toString();
        endCompare += " " + endTime.getText().toString();

        boolean startBeforeEnd = false;

        try {
            startBeforeEnd = sdfCombine.parse(startCompare).before(sdfCombine.parse(endCompare));
        } catch (ParseException p) {
            Log.d(MainActivity.DEBUG_TAG, "Parse Exception caught.");
        }

        if(!(startBeforeEnd)) {
            Toast.makeText(addEventActivity.getApplicationContext(),
                    "End Time cannot be before Start Time.", Toast.LENGTH_SHORT).show();
            return false;
        }

        inputTest = venue.getText().toString();
        if(inputTest.isEmpty()) {
            Toast.makeText(addEventActivity.getApplicationContext(),
                    "Venue cannot be empty.", Toast.LENGTH_SHORT).show();
            return false;
        }

        if(isEventOverlap(passedInEventsArray) == true) {
            Toast.makeText(addEventActivity.getApplicationContext(),
                    "Event Time overlaps with an existing Event!", Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

    private Event createEventFromInput(){

        String titleResult = title.getText().toString();
        String venueResult = venue.getText().toString();
        String notesResult = notes.getText().toString();
        String geoLocResult = geoLoc.getText().toString();
        ArrayList<String> attendeesResult = selectedContactsArray;
        String startDateTimeString = startDate.getText().toString() + " " + startTime.getText().toString();
        String endDateTimeString = endDate.getText().toString() + " " + endTime.getText().toString();

        Date startDateTimeResult = null;
        try {
            startDateTimeResult = sdfCombine.parse(startDateTimeString);
        } catch (ParseException p) {
            Log.d(MainActivity.DEBUG_TAG, "Parse Exception caught.");
        }

        Date endDateTimeResult = null;
        try {
            endDateTimeResult = sdfCombine.parse(endDateTimeString);
        } catch (ParseException p) {
            Log.d(MainActivity.DEBUG_TAG, "Parse Exception caught.");
        }

        Event createdEvent = new Event(titleResult, venueResult, notesResult, attendeesResult, startDateTimeResult, endDateTimeResult, geoLocResult, generateEventId());

        return createdEvent;
    }

    private Event createEventFromInput(String eventId){

        String titleResult = title.getText().toString();
        String venueResult = venue.getText().toString();
        String notesResult = notes.getText().toString();
        String geoLocResult = geoLoc.getText().toString();
        ArrayList<String> attendeesResult = selectedContactsArray;
        String startDateTimeString = startDate.getText().toString() + " " + startTime.getText().toString();
        String endDateTimeString = endDate.getText().toString() + " " + endTime.getText().toString();

        Date startDateTimeResult = null;
        try {
            startDateTimeResult = sdfCombine.parse(startDateTimeString);
        } catch (ParseException p) {
            Log.d(MainActivity.DEBUG_TAG, "Parse Exception caught.");
        }

        Date endDateTimeResult = null;
        try {
            endDateTimeResult = sdfCombine.parse(endDateTimeString);
        } catch (ParseException p) {
            Log.d(MainActivity.DEBUG_TAG, "Parse Exception caught.");
        }

        Event createdEvent = new Event(titleResult, venueResult, notesResult, attendeesResult, startDateTimeResult, endDateTimeResult, geoLocResult, eventId);

        return createdEvent;
    }

    private boolean isEventOverlap(ArrayList<Event> passedEvents){

        String startDateTimeString = startDate.getText().toString() + " " + startTime.getText().toString();
        Date startDate = null;
        try {
            startDate = sdfCombine.parse(startDateTimeString);
        } catch (ParseException p) {
            Log.d(MainActivity.DEBUG_TAG, "Parse Exception caught.");
        }

        String endDateTimeString = endDate.getText().toString() + " " + endTime.getText().toString();
        Date endDate = null;
        try {
            endDate = sdfCombine.parse(endDateTimeString);
        } catch (ParseException p) {
            Log.d(MainActivity.DEBUG_TAG, "Parse Exception caught.");
        }

        if(sentIntent.hasExtra(MainActivity.PASSED_EVENT)) {
            Event passedEvent = sentIntent.getParcelableExtra(MainActivity.PASSED_EVENT);
            for (Event event : passedEvents) {
                if (!(startDate.before(event.getStartDate()) && endDate.before(event.getStartDate()) ||
                        startDate.after(event.getEndDate()) && endDate.after(event.getEndDate()))) {
                    if (passedEvent.getEventId().equals(event.getEventId())) {
                        return false;
                    }
                    return true;
                }
            }
        } else if (passedEvents.size()> 0){
            for (Event event : passedEvents) {
                if(!(startDate.before(event.getStartDate()) && endDate.before(event.getStartDate()) ||
                        startDate.after(event.getEndDate())&& endDate.after(event.getEndDate())) ){
                    return true;
                }
            }
        } else {
            return false;
        }
        return false;
    }

    private String generateEventId(){

        StringBuilder builder = new StringBuilder();
        final String ALPHA_STRING = "abcdefghijklmnopqrstuvwxyz";
        final String NUM_STRING = "1234567890";
            for (int i = 0; i < 4 ; i++) {
                int randChar = (int)(Math.random()*ALPHA_STRING.length());
                builder.append(ALPHA_STRING.charAt(randChar));
                randChar = (int)(Math.random()*NUM_STRING.length());
                builder.append(NUM_STRING.charAt(randChar));
            }
        return builder.toString();
    }

}
