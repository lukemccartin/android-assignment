package com.example.luke.assignment1.controller;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.example.luke.assignment1.R;
import com.example.luke.assignment1.model.Event;
import com.example.luke.assignment1.model.Singleton;
import com.example.luke.assignment1.view.AddEventActivity;
import com.example.luke.assignment1.view.BasicListAdapter;
import com.example.luke.assignment1.view.CalendarFragment;
import com.example.luke.assignment1.view.MainActivity;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;


/**
 * Created by Luke on 18/08/2016.
 */
public class CalendarFragmentController implements AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener, View.OnClickListener {

    private Singleton singleton;
    private ArrayList<Event> eventsArray;
    private CalendarFragment calendarFragment;
    private final DateFormat titleDateFormat = new SimpleDateFormat("MMMM yyy");
    private SimpleDateFormat sdfCombine = new SimpleDateFormat("dd MMMM yyyy");
    private SimpleDateFormat sdfMatch = new SimpleDateFormat("dd/MM/yyyy");
    private Calendar currentMonthCalendar;

    public CalendarFragmentController(CalendarFragment calendarFragment) {
        this.calendarFragment = calendarFragment;
        singleton = Singleton.getInstance();
        eventsArray = singleton.getEventsArray();
        currentMonthCalendar = Calendar.getInstance();
    }

    public void goToCurrentMonth() {
        currentMonthCalendar = Calendar.getInstance();
        calendarFragment.setMonthTitle((titleDateFormat.format(currentMonthCalendar.getTime())));
        calendarFragment.getAdapter().generateCalendarArray(currentMonthCalendar);
        notifyEventChange();
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

        TextView icon = (TextView) view.findViewById(R.id.rowBasicGridTextView);
        String selectedDate = String.valueOf(icon.getText()) + " " + calendarFragment.getMonthTitle();
        final ArrayList<Event> listForCurrentDate = new ArrayList<>();
        ArrayList<String> displayableEventsAsStrings = new ArrayList<>();

        try {
            Date combinedDate = sdfCombine.parse(selectedDate);
            String combine = sdfMatch.format(combinedDate);
            for (Event event : eventsArray) {
                if (combine.equals(sdfMatch.format(event.getStartDate()))) {
                    listForCurrentDate.add(event);
                    displayableEventsAsStrings.add(event.getTitle());
                }
            }
        } catch (ParseException p) {
            Log.d(MainActivity.DEBUG_TAG, "Parse Exception " + p + " Caught");
        }
        for (String string : displayableEventsAsStrings) {
            Log.d(MainActivity.DEBUG_TAG, string + " In new Array.");
        }

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                calendarFragment.getActivity(),
                android.R.layout.select_dialog_singlechoice);
        arrayAdapter.addAll(displayableEventsAsStrings);

        if (!(listForCurrentDate.isEmpty())) {
            AlertDialog.Builder dialog = new AlertDialog.Builder(calendarFragment.getActivity());
            dialog.setTitle(R.string.CalendarEditTitle)
                    .setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Event event = listForCurrentDate.get(i);
                            Intent editIntent = new Intent(calendarFragment.getActivity(), AddEventActivity.class);
                            editIntent.putExtra(MainActivity.PASSED_EVENT, event);
                            editIntent.putParcelableArrayListExtra(MainActivity.EVENTS_ARRAY, eventsArray);
                            calendarFragment.startActivityForResult(editIntent, MainActivity.DETAIL_REQUEST_3);
                        }
                    })
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogInterface, int i) {
                        }
                    }).create();
            dialog.show();
        }
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {

        TextView icon = (TextView) view.findViewById(R.id.rowBasicGridTextView);
        String selectedDate = String.valueOf(icon.getText()) + " " + calendarFragment.getMonthTitle();
        ArrayList<Event> listForCurrentDate = new ArrayList<>();
        final ArrayList<String> displayableEventsAsStrings = new ArrayList<>();

        try {
            Date combinedDate = sdfCombine.parse(selectedDate);
            String combine = sdfMatch.format(combinedDate);
            for (Event event : eventsArray) {
                if (combine.equals(sdfMatch.format(event.getStartDate()))) {
                    listForCurrentDate.add(event);
                    displayableEventsAsStrings.add(event.getTitle());
                }
            }
        } catch (ParseException p) {
            Log.d(MainActivity.DEBUG_TAG, "Parse Exception " + p + " Caught");
        }

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                calendarFragment.getActivity(),
                android.R.layout.select_dialog_singlechoice);
        arrayAdapter.addAll(displayableEventsAsStrings);

        if (!(listForCurrentDate.isEmpty())) {
            AlertDialog.Builder dialog = new AlertDialog.Builder(calendarFragment.getActivity());
            dialog.setTitle(R.string.CalendarDeleteTitle)
                    .setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, final int i) {
                            AlertDialog.Builder dialog = new AlertDialog.Builder(calendarFragment.getActivity());
                            dialog.setTitle(R.string.DialogTitle)
                                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            Event eventToRemove = null;
                                            for (Event event : eventsArray){
                                                if (displayableEventsAsStrings.get(i).equals(event.getTitle())) {
                                                    eventToRemove = event;
                                                }
                                            }
                                            if(eventToRemove != null) {
                                                eventsArray.remove(eventToRemove);
                                            }
                                            calendarFragment.getAdapter().notifyDataSetChanged();
                                            ((MainActivity)calendarFragment.getActivity()).updateFragments();
                                        }
                                    })
                                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                        }
                                    }).create();
                            dialog.show();
                        }
                    })
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogInterface, int i) {
                        }
                    }).create();
            dialog.show();
        }
        return true;
    }


    public void notifyEventChange(){
        calendarFragment.getAdapter().notifyDataSetChanged();
    }

    public ArrayList<Event> getEventsArray() {
        return eventsArray;
    }

    @Override
    public void onClick(View view) {
        if(view == view.findViewById(R.id.previousButton)){
            currentMonthCalendar.set(Calendar.MONTH, currentMonthCalendar.get(currentMonthCalendar.MONTH) -1);
            calendarFragment.setMonthTitle(titleDateFormat.format(currentMonthCalendar.getTime()));
            calendarFragment.getAdapter().generateCalendarArray(currentMonthCalendar);
            notifyEventChange();
        } else if (view == view.findViewById(R.id.nextButton)){
            currentMonthCalendar.set(Calendar.MONTH, currentMonthCalendar.get(currentMonthCalendar.MONTH) +1);
            calendarFragment.setMonthTitle(titleDateFormat.format(currentMonthCalendar.getTime()));
            calendarFragment.getAdapter().generateCalendarArray(currentMonthCalendar);
            notifyEventChange();
        }
    }

    public void handleActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == MainActivity.DETAIL_REQUEST_3) {
            if (resultCode == MainActivity.RESULT_OK) {
                Event newEvent = data.getExtras().getParcelable(MainActivity.RETURN_EVENT);

                Event eventForRemoval = null;
                for (Event event : singleton.getEventsArray()) {
                    if (newEvent.getEventId().equals(event.getEventId())) {
                        eventForRemoval = event;
                    }
                }
                if (!(eventForRemoval == null)) {
                    singleton.getEventsArray().remove(eventForRemoval);
                }
                singleton.getEventsArray().add(newEvent);
                ((MainActivity)calendarFragment.getActivity()).updateFragments();
            }
        }
    }

}

