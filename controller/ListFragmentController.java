package com.example.luke.assignment1.controller;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;

import com.example.luke.assignment1.R;
import com.example.luke.assignment1.model.Event;
import com.example.luke.assignment1.model.Singleton;
import com.example.luke.assignment1.view.AddEventActivity;
import com.example.luke.assignment1.view.ListFragment;
import com.example.luke.assignment1.view.MainActivity;

import java.util.ArrayList;

/**
 * Created by Luke on 18/08/2016.
 */
public class ListFragmentController implements AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener {

    private Singleton singleton;
    private ArrayList<Event> eventsArray;
    private ListFragment listFragment;


    public ListFragmentController(ListFragment listFragment) {
        singleton = Singleton.getInstance();
        eventsArray = singleton.getEventsArray();
        this.listFragment = listFragment;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

        Event event = eventsArray.get(i);
        Intent editIntent = new Intent(listFragment.getActivity().getApplicationContext(), AddEventActivity.class);
        editIntent.putExtra(MainActivity.PASSED_EVENT, event);
        editIntent.putParcelableArrayListExtra(MainActivity.EVENTS_ARRAY, eventsArray);
        listFragment.startActivityForResult(editIntent, MainActivity.DETAIL_REQUEST_3);
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {

        final Event event = eventsArray.get(i);

        AlertDialog.Builder dialog = new AlertDialog.Builder(listFragment.getActivity());
        dialog.setTitle(R.string.DialogTitle)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        eventsArray.remove(event);
                        listFragment.getAdapter().notifyDataSetChanged();
                        ((MainActivity)listFragment.getActivity()).updateFragments();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                    }
                }).create();
        dialog.show();

        return true;
    }

    public void updateListOrder(){

        boolean isAscending = true;
        for (int x = 0; x < eventsArray.size(); x++) {
            for (int y = 1 + x; y < eventsArray.size(); y++) {
                if(eventsArray.get(x).getStartDate().after(eventsArray.get(y).getStartDate())){
                    isAscending = false;
                }
            }
        }

        if(!(isAscending)){
            toggleAscending();
        } else {
            toggleDescending();
        }
    }

    public void toggleAscending() {

        Event[] replacementArray = new Event[eventsArray.size()];
        for (int i = 0; i < eventsArray.size() ; i++) {
            replacementArray[i] = eventsArray.get(i);
        }

        Event eventHolder = null;
        for (int i = 0; i <= replacementArray.length - 2; i++) {
            for (int j = 0; j < replacementArray.length - 1 - i; j++) {
                if (replacementArray[j+1].getStartDate().before(replacementArray[j].getStartDate())) {
                    eventHolder = replacementArray[j];
                    replacementArray[j] = replacementArray[j+1];
                    replacementArray[j + 1] = eventHolder;
                }
            }
        }

        eventsArray.clear();
        for (Event event:replacementArray) {
            eventsArray.add(event);
        }
        listFragment.getAdapter().notifyDataSetChanged();
    }

    public void toggleDescending() {

        Event[] replacementArray = new Event[eventsArray.size()];
        for (int i = 0; i < eventsArray.size() ; i++) {
            replacementArray[i] = eventsArray.get(i);
        }

        Event eventHolder = null;

        for (int i = 0; i <= replacementArray.length - 2; i++) {
            for (int j = 0; j < replacementArray.length - 1 - i; j++) {
                if (replacementArray[j+1].getStartDate().after(replacementArray[j].getStartDate())) {
                    eventHolder = replacementArray[j];
                    replacementArray[j] = replacementArray[j+1];
                    replacementArray[j + 1] = eventHolder;
                }
            }
        }

        eventsArray.clear();
        for (Event event:replacementArray) {
            eventsArray.add(event);
        }
        listFragment.getAdapter().notifyDataSetChanged();
    }

    public void handleActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == MainActivity.DETAIL_REQUEST_3) {
            if (resultCode == MainActivity.RESULT_OK) {
                Event newEvent = data.getExtras().getParcelable(MainActivity.RETURN_EVENT);

                Event eventForRemoval = null;
                for (Event event : singleton.getEventsArray()) {
                    if (newEvent.getEventId().equals(event.getEventId())) {
                        eventForRemoval = event;
                    }
                }
                if (!(eventForRemoval == null)) {
                    singleton.getEventsArray().remove(eventForRemoval);
                }
                singleton.getEventsArray().add(newEvent);
                toggleAscending();
            }
        }
    }

    public ArrayList<Event> getEventsArray() {
        return eventsArray;
    }

    public void notifyEventChange(){
        listFragment.getAdapter().notifyDataSetChanged();
    }
}
