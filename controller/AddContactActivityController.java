package com.example.luke.assignment1.controller;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.ListView;

import com.example.luke.assignment1.view.AddContactActivity;
import com.example.luke.assignment1.view.MainActivity;

import java.util.ArrayList;

/**
 * Created by Luke on 18/08/2016.
 */
public class AddContactActivityController implements AdapterView.OnItemClickListener, View.OnClickListener {

    private AddContactActivity addContactActivity;
    private ArrayList<String> importedContactsArray;
    private ListView contactsList;

    public AddContactActivityController(AddContactActivity addContactActivity, ArrayList<String> importedContactsArray, ListView contactsList) {
        this.addContactActivity = addContactActivity;
        this.importedContactsArray = importedContactsArray;
        this.contactsList = contactsList;
    }

    @Override
    public void onClick(View view) {

        final int numberOfRows = importedContactsArray.size();
        ArrayList<String> chosenContactsArray = new ArrayList<String>();

        for (int i = 0; i < numberOfRows; i++) {
            String chosen = importedContactsArray.get(i);
            View currentRow = contactsList.getChildAt(i);
            CheckBox checking  = (CheckBox) currentRow.findViewWithTag(i);
            if(checking.isChecked()){
                chosenContactsArray.add(chosen);
            } else {
                chosenContactsArray.remove(chosen);
            }
        }
        Intent returnData = new Intent();
        returnData.putExtra(MainActivity.RETURN_EVENT,chosenContactsArray);
        addContactActivity.setResult(MainActivity.RESULT_OK, returnData);
        addContactActivity.finish();
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        CheckBox check = (CheckBox)  adapterView.findViewWithTag(i);
        if(!(check.isChecked())){
            check.setChecked(true);
        } else {
            check.setChecked(false);
        }
    }


}
