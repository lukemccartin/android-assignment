package com.example.luke.assignment1.model;

import java.util.ArrayList;

/**
 * Created by Luke on 15/08/2016.
 **/
public class Singleton {

    private static Singleton instance = null;
    private ArrayList<Event> eventsArray;

    protected Singleton(){
        eventsArray = new ArrayList<>();
    }

    public static Singleton getInstance(){
        if(instance == null){
            instance = new Singleton();
        }
        return instance;
    }

    public ArrayList<Event> getEventsArray(){
        return eventsArray;
    }

    public void setEventsArray (ArrayList<Event> eventsArray) {
        this.eventsArray = eventsArray;
    }

}
