package com.example.luke.assignment1.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Luke on 15/09/2016.
 */
public class EventDataSource {

    public static final String DATA_SOURCE_DEBUG = "DATA_SOURCE_LOG";

    SQLiteOpenHelper dbHelper;
    SQLiteDatabase database;

    private SimpleDateFormat sdfDate = new SimpleDateFormat("dd/MM/yyyy");
    private SimpleDateFormat sdfTime = new SimpleDateFormat("HH:mm");
    private SimpleDateFormat sdfCombine = new SimpleDateFormat("dd/MM/yyyy HH:mm");

    private static final String[] allColumnsEventTable = {
            EventDBOpenHelper.COLUMN_EVENT_ID,
            EventDBOpenHelper.COLUMN_TITLE,
            EventDBOpenHelper.COLUMN_START_DATE,
            EventDBOpenHelper.COLUMN_END_DATE,
            EventDBOpenHelper.COLUMN_VENUE,
            EventDBOpenHelper.COLUMN_GEOLOC,
            EventDBOpenHelper.COLUMN_NOTE
    };

    private static final String[] allColumnsAttendeeTable = {
            EventDBOpenHelper.COLUMN_NAME,
            EventDBOpenHelper.COLUMN_EVENT_ID
    };

    private final String retrieveAttendeesQuery = "SELECT " + EventDBOpenHelper.COLUMN_NAME + " FROM " + EventDBOpenHelper.TABLE_ATTENDEES +
            " a INNER JOIN " + EventDBOpenHelper.TABLE_EVENTS + " b ON a." +
            EventDBOpenHelper.COLUMN_EVENT_ID + "=b." + EventDBOpenHelper.COLUMN_EVENT_ID + " WHERE a." + EventDBOpenHelper.COLUMN_EVENT_ID + " = " ;

    public EventDataSource(Context context) {
        dbHelper = new EventDBOpenHelper(context);
    }

    public void open() {
        Log.d(DATA_SOURCE_DEBUG, "Open Called.");
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        Log.d(DATA_SOURCE_DEBUG, "Close Called.");
        dbHelper.close();
    }

    public void create(Event event) {
        ContentValues values = new ContentValues();
        values.put(EventDBOpenHelper.COLUMN_EVENT_ID, event.getEventId());
        values.put(EventDBOpenHelper.COLUMN_TITLE, event.getTitle());

        String eventStartDate = sdfDate.format(event.getStartDate());
        String eventStartTime = sdfTime.format(event.getStartDate());
        String startDate = eventStartDate + " " + eventStartTime;

        String eventEndDate = sdfDate.format(event.getEndDate());
        String eventEndTime = sdfTime.format(event.getEndDate());
        String endDate = eventEndDate + " " + eventEndTime;

        values.put(EventDBOpenHelper.COLUMN_START_DATE, startDate);
        values.put(EventDBOpenHelper.COLUMN_END_DATE, endDate);

        values.put(EventDBOpenHelper.COLUMN_VENUE, event.getVenue());
        values.put(EventDBOpenHelper.COLUMN_GEOLOC, event.getGeoLocation());
        values.put(EventDBOpenHelper.COLUMN_NOTE, event.getNote());

        database.insert(EventDBOpenHelper.TABLE_EVENTS, null, values);
        values.clear();

        ArrayList<String> attendees = event.getAttendees();
        for (String attendee : attendees) {
            values.put(EventDBOpenHelper.COLUMN_NAME, attendee);
            values.put(EventDBOpenHelper.COLUMN_EVENT_ID, event.getEventId());
            database.insert(EventDBOpenHelper.TABLE_ATTENDEES, null, values);
            values.clear();
        }
    }

    public void clearDB(){
        database.delete(EventDBOpenHelper.TABLE_EVENTS, null, null );
        database.delete(EventDBOpenHelper.TABLE_ATTENDEES, null, null );
    }

    public ArrayList<Event> retrieveAll() {
        ArrayList<Event> events = new ArrayList<Event>();

        open();
        Cursor cursor = database.query(EventDBOpenHelper.TABLE_EVENTS, allColumnsEventTable, null, null, null, null, null);
        if (cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                String eventId = cursor.getString(cursor.getColumnIndex(EventDBOpenHelper.COLUMN_EVENT_ID));
                String title = cursor.getString(cursor.getColumnIndex(EventDBOpenHelper.COLUMN_TITLE));

                String startDateString = cursor.getString(cursor.getColumnIndex(EventDBOpenHelper.COLUMN_START_DATE));
                Date startDate = null;
                try {
                    startDate = sdfCombine.parse(startDateString);
                } catch (ParseException p) {
                    Log.d("DEBUG", "Parse Exception caught.");
                }

                String endDateString = cursor.getString(cursor.getColumnIndex(EventDBOpenHelper.COLUMN_END_DATE));
                Date endDate = null;
                try {
                    endDate = sdfCombine.parse(endDateString);
                } catch (ParseException p) {
                    Log.d("DEBUG", "Parse Exception caught.");
                }

                String venue = cursor.getString(cursor.getColumnIndex(EventDBOpenHelper.COLUMN_VENUE));
                String geoLoc = cursor.getString(cursor.getColumnIndex(EventDBOpenHelper.COLUMN_GEOLOC));
                String note = cursor.getString(cursor.getColumnIndex(EventDBOpenHelper.COLUMN_NOTE));

                ArrayList<String> attendees = new ArrayList<>();

                String query = retrieveAttendeesQuery.concat("'"+eventId+"';");

                Cursor attendeeCursor = database.rawQuery(query, null);
                if (attendeeCursor.getCount() > 0) {
                    while (attendeeCursor.moveToNext()) {
                        String name = attendeeCursor.getString(attendeeCursor.getColumnIndex(EventDBOpenHelper.COLUMN_NAME));
                        attendees.add(name);
                    }
                }

                Event event = new Event(title, venue, note, attendees, startDate, endDate, geoLoc, eventId);
                events.add(event);
            }
        }
        return events;
    }
}
