package com.example.luke.assignment1.model;

import android.Manifest;
import android.app.IntentService;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.example.luke.assignment1.R;
import com.example.luke.assignment1.view.MainActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.InputMismatchException;

/**
 * Created by Luke on 24/09/2016.
 */
public class LocationAlarmService extends IntentService {

    private final String distanceMatrixURLString = "https://maps.googleapis.com/maps/api/distancematrix/json?";
    private final String distanceMatrixOrigins = "origins=";
    private final String distanceMatrixDestinations = "&destinations=";
    private final String distanceMatrixKey = "&key=AIzaSyCgFPSShZ4LP2QLP27h_b7OiCzpDVsXXd0";
    private LocationManager locationManager;
    private LocationListener locationListener;
    private Location currentLocation;
    private ArrayList<Event> eventArray;

    public LocationAlarmService() {
        super("LocationAlarmService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.d(MainActivity.DEBUG_TAG, "Alarm Service called");


        currentLocation = null;
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                Log.d(MainActivity.DEBUG_TAG, "onLocationChanged called, Location is: " + String.valueOf(location));
                Location myLocation = location;
                Log.d(MainActivity.DEBUG_TAG, myLocation.toString());
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {
                Log.d(MainActivity.DEBUG_TAG, s);
            }

            @Override
            public void onProviderEnabled(String s) {
                Log.d(MainActivity.DEBUG_TAG, s);

            }

            @Override
            public void onProviderDisabled(String s) {
                Log.d(MainActivity.DEBUG_TAG, s);
            }
        };

        if (hasConnection()) {
            if (hasExistingEvents()) {
                if (hasPreviousGPSLocation()) {
                    Calendar calendar = Calendar.getInstance();
                    Date currentDateTime = calendar.getTime();
                    int notifyId = 1111;
                    for(Event event: eventArray) {
                        Date eventStartDate = event.getStartDate();
                        if(currentDateTime.before(eventStartDate)){
                            String eventGeoLoc = event.getGeoLocation();
                            long timeToLocation = -1;
                            String[] returnValues = null;
                            try {
                                returnValues = connectToDistanceMatrix(eventGeoLoc);
                                timeToLocation = Long.parseLong(returnValues[0]);
                            } catch (InputMismatchException | JSONException e) {
                                Log.d(MainActivity.DEBUG_TAG, e.getMessage());

                            }
                            if(timeToLocation > 0){
                                long timeDifference = eventStartDate.getTime() -
                                        currentDateTime.getTime();
                                Log.d(MainActivity.DEBUG_TAG, event.getTitle());
                                if(isAlarmNeeded(timeDifference, timeToLocation)) {
                                    if(returnValues != null){
                                        buildNotification(event, timeDifference, notifyId, returnValues[1]);
                                    }
                                }
                            } else {
                                Log.d(MainActivity.DEBUG_TAG, "Unhandled Error caught when " +
                                        "connecting to distance matrix.");
                            }
                        }
                        notifyId ++;
                    }
                } else {
                    Log.d(MainActivity.DEBUG_TAG, "No Previous GPS Location Available.");
                }
            } else {
                Log.d(MainActivity.DEBUG_TAG, "No Created Events In System.");

            }
        } else {
            Log.d(MainActivity.DEBUG_TAG, "No Connection Available.");
        }
    }

    private boolean hasConnection() {
        boolean isConnected;

        ConnectivityManager cm = (ConnectivityManager)
                this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();

        return isConnected;
    }

    private String[] connectToDistanceMatrix(String eventGeoLoc) throws JSONException {
        HttpURLConnection connection;
        InputStream inputStream;
        String[] returnValues = new String[2];
        long numValue;

        double lat = currentLocation.getLatitude();
        double lng = currentLocation.getLongitude();

        try {
            URL url = new URL(distanceMatrixURLString + distanceMatrixOrigins + lat + "," + lng +
                    distanceMatrixDestinations + eventGeoLoc + distanceMatrixKey);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Accept", "text/html");
            connection.connect();
            inputStream = connection.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            StringBuilder stringBuilder = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line);
            }
            String resultTotal = stringBuilder.toString();
            try {
                JSONObject returnedObject = new JSONObject(resultTotal);
                JSONArray array = returnedObject.getJSONArray("rows");
                returnedObject = array.getJSONObject(0);
                array = returnedObject.getJSONArray("elements");
                returnedObject = array.getJSONObject(0);
                returnedObject = returnedObject.getJSONObject("duration");
                numValue = returnedObject.getLong("value");
                Log.d(MainActivity.DEBUG_TAG, "Returned time value: " + String.valueOf(numValue));
                String textValue = returnedObject.getString("text");
                Log.d(MainActivity.DEBUG_TAG, "Returned text value: " + textValue);
                returnValues[0] = String.valueOf(numValue);
                returnValues[1] = textValue;

            } catch (JSONException e) {
                Log.d(MainActivity.DEBUG_TAG, e + " Caught.");
                throw new JSONException("No Duration for GPS Location available.");
            }
        } catch (IOException e) {
            Log.d(MainActivity.DEBUG_TAG, e + " Caught during" +
                    " Alarm distance lookup");
        }
        return returnValues;
    }

    private boolean hasPreviousGPSLocation() {

        boolean hasLocation;

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {

            // TODO: Consider calling
//                ActivityCompat#requestPermissions
//             here to request the missing permissions, and then overriding
//               public void onRequestPermissionsResult(int requestCode, String[] permissions,
//                                                      int[] grantResults)
//             to handle the case where the user grants the permission. See the documentation
//             for ActivityCompat#requestPermissions for more details.
            return false;
        }

        if(locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
                || locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
            if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
                currentLocation = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            }
            if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) && currentLocation == null) {
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
                currentLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            }
            if (currentLocation != null){
                hasLocation = true;
            } else {
                hasLocation = false;
            }
        } else {
            hasLocation = false;
        }
    return hasLocation;
    }

    private boolean hasExistingEvents() {

        boolean hasEvents;
        Singleton singleton = Singleton.getInstance();
        if(singleton != null){
            ArrayList<Event> events = singleton.getEventsArray();
            eventArray = new ArrayList<>();
            eventArray.addAll(events);
            if(eventArray.size() == 0) {
                Log.d(MainActivity.DEBUG_TAG, "Singleton array empty");
                hasEvents = false;
            } else {
                hasEvents = true;
            }
        } else {
            EventDataSource eventDataSource = new EventDataSource(this);
            eventArray = eventDataSource.retrieveAll();
            if (eventArray.size() == 0) {
                Log.d(MainActivity.DEBUG_TAG, "Database array empty");
                hasEvents = false;
            } else {
                hasEvents = true;
            }
        }
        return  hasEvents;
    }

    private boolean isAlarmNeeded(long timeDifference, long timeToLocationInSeconds) {

        boolean isAlarmNeeded;
        final long secondsInMillis = 1000;
        long timeDifferenceInSeconds;
        timeDifferenceInSeconds = timeDifference / secondsInMillis;

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        String stringThreshold = sharedPreferences.getString(getString(R.string.timeThresholdKey),"900");

        long timeThreshold = Long.parseLong(stringThreshold);
        if(timeToLocationInSeconds + timeThreshold >= timeDifferenceInSeconds) {
            isAlarmNeeded = true;
        } else {
            isAlarmNeeded = false;
        }
        return isAlarmNeeded;
    }

    private void buildNotification(Event event, long timeDifference, int notifyId, String distanceText) {
        String contentText;
        String textValue = distanceText;
            long days, hours, minutes, seconds;
            final long secondsInMillis = 1000;
            final long minutesInMillis = secondsInMillis * 60;
            final long hoursInMillis = minutesInMillis * 60;
            final long daysInMillis = hoursInMillis * 24;

            days = timeDifference / daysInMillis;
            timeDifference = timeDifference % daysInMillis;
            hours = timeDifference / hoursInMillis;
            timeDifference = timeDifference % hoursInMillis;
            minutes = timeDifference / minutesInMillis;
            timeDifference = timeDifference % minutesInMillis;
            seconds = timeDifference / secondsInMillis;

            if(days == 0 && hours == 0 && minutes == 0) {
                contentText =  seconds + " secs. " + textValue + " away.";
            } else if (days == 0 && hours == 0) {
                contentText =  minutes
                        + " mins, " + seconds + " secs. " + textValue + " away.";
            } else if(days == 0) {
                contentText =  hours + " hrs, " + minutes
                        + " mins, " + seconds + " secs. " + textValue + " away.";
            } else {
                contentText = days + " days, " + hours + " hrs, " + minutes
                        + " mins, " + seconds + " secs. " + textValue + " away.";
            }
        NotificationCompat.Builder eventBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.common_plus_signin_btn_icon_dark)
                .setContentTitle(event.getTitle())
                .setContentText("Starts in: " + contentText)
                .setColor(getResources().getColor(R.color.colorAccent));
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(notifyId, eventBuilder.build());
    }

}
