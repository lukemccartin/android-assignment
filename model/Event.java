package com.example.luke.assignment1.model;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Luke on 4/08/2016.
 */
public class Event implements Parcelable {

    private String title;
    private Date startDate;
    private Date endDate;
    private String venue;
    private String geoLocation;
    private String note;
    private ArrayList<String> attendees;
    private String eventId;

    public Event(String title, String venue, String note, ArrayList<String> attendees, Date startDate, Date endDate, String geoLocation, String eventId){
        this.title = title;
        this.venue = venue;
        this.note = note;
        this.attendees = attendees;
        this.startDate = startDate;
        this.endDate = endDate;
        this.geoLocation = geoLocation;
        this.eventId = eventId;
    }

    public String getTitle(){
        return this.title;
    }

    public Date getStartDate(){
        return startDate;
    }

    public Date getEndDate(){
        return endDate;
    }

    public String getVenue(){
        return venue;
    }

    public String getGeoLocation(){
        return geoLocation;
    }

    public String getNote(){
        return note;
    }

    public ArrayList<String> getAttendees(){
        return attendees;
    }

    public String getEventId(){
        return eventId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(title);
        parcel.writeString(venue);
        parcel.writeString(note);
        parcel.writeList(attendees);
        parcel.writeLong(startDate.getTime());
        parcel.writeLong(endDate.getTime());
        parcel.writeString(geoLocation);
        parcel.writeString(eventId);
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public Event createFromParcel(Parcel in) {
            return new Event(in);
        }

        @Override
        public Event[] newArray(int i) {
            return new Event[i];
        }
    };

    public Event(Parcel in) {
        title = in.readString();
        venue = in.readString();
        note = in.readString();
        attendees = in.readArrayList(null);
        startDate = new Date(in.readLong());
        endDate = new Date(in.readLong());
        geoLocation = in.readString();
        eventId = in.readString();
    }
}
