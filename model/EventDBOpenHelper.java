package com.example.luke.assignment1.model;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Luke on 15/09/2016.
 */
public class EventDBOpenHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "event.db";
    private static final int DATABASE_VERSION = 1;

    // Event Table
    public static final String TABLE_EVENTS = "events";
    public static final String COLUMN_TITLE = "title";
    public static final String COLUMN_START_DATE = "startDate"; // These are date objects that need to be converted to Strings and then back.
    public static final String COLUMN_END_DATE = "endDate"; // These are date objects that need to be converted to Strings and then back.
    public static final String COLUMN_VENUE = "venue";
    public static final String COLUMN_GEOLOC = "geoLocation";
    public static final String COLUMN_NOTE = "note";
    public static final String COLUMN_EVENT_ID = "eventId";

    // Attendee Table
    public static final String TABLE_ATTENDEES = "attendees"; // Will represent the arrayList of attendees held in Event object
    public static final String COLUMN_NAME = "name";

    private static final String CREATE_EVENT_TABLE =
            "CREATE TABLE " + TABLE_EVENTS + " (" +
                    COLUMN_EVENT_ID + " TEXT PRIMARY KEY, " +
                    COLUMN_TITLE + " TEXT, " +
                    COLUMN_START_DATE + " TEXT, " +
                    COLUMN_END_DATE + " TEXT, " +
                    COLUMN_VENUE + " TEXT, " +
                    COLUMN_GEOLOC + " TEXT, " +
                    COLUMN_NOTE + " TEXT" + ");";

    private static final String CREATE_ATTENDEES_TABLE =
            "CREATE TABLE " + TABLE_ATTENDEES + " (" +
                    COLUMN_NAME + " TEXT, " +
                    COLUMN_EVENT_ID + " TEXT, " +
                    "FOREIGN KEY (" + COLUMN_EVENT_ID + ") REFERENCES "+ TABLE_EVENTS + "(" + COLUMN_EVENT_ID + "), " +
                    "PRIMARY KEY ("+ COLUMN_NAME + ", " + COLUMN_EVENT_ID +"));";

    public EventDBOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_EVENT_TABLE);
        sqLiteDatabase.execSQL(CREATE_ATTENDEES_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_ATTENDEES);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_EVENTS);
        onCreate(sqLiteDatabase);
    }
}
