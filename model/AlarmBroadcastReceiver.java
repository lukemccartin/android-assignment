package com.example.luke.assignment1.model;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.example.luke.assignment1.view.MainActivity;

/**
 * Created by Luke on 24/09/2016.
 */
public class AlarmBroadcastReceiver extends BroadcastReceiver {

    AlarmManager alarmManager;

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {
            Intent locationAlarmServiceIntent = new Intent(context, LocationAlarmService.class);
            alarmManager = (AlarmManager) context.getSystemService(Service.ALARM_SERVICE);
            alarmManager.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,
                    MainActivity.FIVE_MINUTES, MainActivity.FIVE_MINUTES,
                    PendingIntent.getService(context, MainActivity.ALARM_SERVICE_ID_CODE,
                            locationAlarmServiceIntent,
                            PendingIntent.FLAG_UPDATE_CURRENT));

        }
    }
}
