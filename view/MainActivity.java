package com.example.luke.assignment1.view;


import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.preference.PreferenceManager;
import android.support.design.widget.TabLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.example.luke.assignment1.R;
import com.example.luke.assignment1.controller.CalendarFragmentController;
import com.example.luke.assignment1.controller.ListFragmentController;
import com.example.luke.assignment1.model.Event;
import com.example.luke.assignment1.model.EventDataSource;
import com.example.luke.assignment1.model.LocationAlarmService;
import com.example.luke.assignment1.model.Singleton;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity {

    EventDataSource dataSource;

    public static final String DEBUG_TAG = "DEBUG" ;

    private ListFragment listFragment;
    private CalendarFragment calendarFragment;
    private Singleton singleton;
    private ListFragmentController listFragmentController;
    private CalendarFragmentController calendarFragmentController;
    private AlarmManager alarmManager;

    public static final String RETURN_EVENT = "RETURN_EVENT";
    public static final String PASSED_EVENT = "PASSED_EVENT";
    public static final String EVENTS_ARRAY = "EVENTS_ARRAY";
    public static final int DETAIL_REQUEST = 1;
    public static final int DETAIL_REQUEST_3 = 3;
    public static final int REQUEST_CODE_PREFERENCES = 8;
    public static final int ALARM_SERVICE_ID_CODE = 999;
    public static final long FIVE_MINUTES = 300000;
    // For testing purposes
    public static final long ONE_MINUTE = 60000;

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        singleton = Singleton.getInstance();
        dataSource = new EventDataSource(this);

            ArrayList<Event> eventsArray = dataSource.retrieveAll();
            if(eventsArray.size() > 0) {
                singleton.setEventsArray(eventsArray);
            }

        listFragment = new ListFragment();
        listFragmentController = new ListFragmentController(listFragment);
        calendarFragment = new CalendarFragment();
        calendarFragmentController = new CalendarFragmentController(calendarFragment);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent newIntent = new Intent(MainActivity.this, AddEventActivity.class);
                newIntent.putParcelableArrayListExtra(EVENTS_ARRAY, singleton.getEventsArray());
                startActivityForResult(newIntent, DETAIL_REQUEST);
            }
        });

        setAlarmState();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here.
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            if(mViewPager.getCurrentItem() == 0){
                listFragmentController.updateListOrder();
            }
            if (mViewPager.getCurrentItem() == 1){
                calendarFragmentController.goToCurrentMonth();
            }
            return true;
        } else if (id == R.id.alarm_configure){
            Intent newIntent = new Intent(MainActivity.this, PreferencesActivity.class);
            startActivityForResult(newIntent, REQUEST_CODE_PREFERENCES);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        MenuItem menuItem = menu.findItem(R.id.action_settings);

        if(mViewPager.getCurrentItem() == 0){
            menuItem.setTitle("Toggle List Order");
        } else {
            menuItem.setTitle("Go To Current Month");
        }
        return true;
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            if (position == 0){
                return listFragment;
            }
            else {
                return calendarFragment;
            }
        }

        @Override
        public int getCount() {
            // Show 2 total pages.
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "EVENT LIST";
                case 1:
                    return "EVENT CALENDAR";
            }
            return null;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == DETAIL_REQUEST) {
            if (resultCode == RESULT_OK) {
                Event newEvent = data.getExtras().getParcelable(RETURN_EVENT);
                singleton.getEventsArray().add(newEvent);
                updateFragments();
                listFragmentController.toggleAscending();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void updateFragments() {
        listFragmentController.notifyEventChange();
        calendarFragmentController.notifyEventChange();
    }

    public ListFragmentController getListFragmentController(){
        return listFragmentController;
    }

    public CalendarFragmentController getCalendarFragmentController() {
        return calendarFragmentController;
    }

    @Override
    protected void onPause() {
        super.onPause();

        final ArrayList<Event> eventsToWrite = new ArrayList<>();
        ArrayList<Event> singletonEvents = Singleton.getInstance().getEventsArray();
        for (Event event : singletonEvents) {
            eventsToWrite.add(event);
        }
        Thread thread = new Thread() {
            public void run() {
                dataSource.open();
                dataSource.clearDB();
                for (Event event : eventsToWrite) {
                    dataSource.create(event);
                }
                dataSource.close();
            }
        };
        thread.start();
    }

    private void setAlarmState(){
        Intent locationAlarmServiceIntent = new Intent(this, LocationAlarmService.class);
        alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        alarmManager.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,
                MainActivity.FIVE_MINUTES, FIVE_MINUTES,
                PendingIntent.getService(this, ALARM_SERVICE_ID_CODE,
                        locationAlarmServiceIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT));

    }
}