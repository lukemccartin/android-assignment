package com.example.luke.assignment1.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.luke.assignment1.R;
import com.example.luke.assignment1.model.Event;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Created by Luke on 4/08/2016.
 */
public class EventListAdapter extends ArrayAdapter<Event> {

    private SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");

    public EventListAdapter(Context context, ArrayList<Event> events){
        super(context,0,events);
    }

    @Override
    public View getView(int position, View view, ViewGroup parent){

        Event event = getItem(position);

        if(view == null){
            view = LayoutInflater.from(getContext()).inflate(R.layout.row_events, parent, false);
        }
        TextView title = (TextView)view.findViewById(R.id.rowTitle);
        TextView date = (TextView)view.findViewById(R.id.rowDate);
        TextView attendees = (TextView)view.findViewById(R.id.rowAttendees);

        title.setText(event.getTitle());
        date.setText(sdf.format(event.getStartDate()));
        if(event.getAttendees() == null){
            attendees.setText("Number of Attendees: 0");
        } else {
            attendees.setText("Number of Attendees: " + String.valueOf(event.getAttendees().size()));
        }


        return view;
    }
}
