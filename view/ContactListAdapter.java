package com.example.luke.assignment1.view;

import android.content.Context;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.example.luke.assignment1.R;
import java.util.ArrayList;


/**
 * Created by Luke on 11/08/2016.
 */
public class ContactListAdapter extends ArrayAdapter <Pair> {

    public ContactListAdapter(Context context, ArrayList<Pair> contacts) {
        super(context, 0, contacts);
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {

        Pair contact = getItem(position);
        if(view == null){
            view = LayoutInflater.from(getContext()).inflate(R.layout.row_contact, parent, false);
        }

        TextView contactName = (TextView) view.findViewById(R.id.contactName);
        contactName.setText((String)contact.second);

        CheckBox checked = (CheckBox) view.findViewById(R.id.contactCheckBox);
        checked.setTag(position);

        if(contact.first.equals(1)) {
            checked.setChecked(true);
        } else if (contact.first.equals(0)) {
            checked.setChecked(false);
        }

        return view;
    }
}
