package com.example.luke.assignment1.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import com.example.luke.assignment1.R;
import com.example.luke.assignment1.controller.ListFragmentController;


/**
 * Created by Luke on 4/08/2016.
 */
public class ListFragment extends Fragment {

    private View rootView;
    private ListView eventList;
    private EventListAdapter adapter;
    private ListFragmentController listFragmentController;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        MainActivity mainActivity = (MainActivity) getActivity();
        listFragmentController = mainActivity.getListFragmentController();
        rootView = inflater.inflate(R.layout.fragment_list, container, false);
        eventList = (ListView)rootView.findViewById(R.id.eventList);
        adapter = new EventListAdapter(getActivity().getApplicationContext(), listFragmentController.getEventsArray());
        eventList.setAdapter(adapter);
        eventList.setOnItemClickListener(listFragmentController);
        eventList.setOnItemLongClickListener(listFragmentController);

        return rootView;
    }

    public EventListAdapter getAdapter() {
        return adapter;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        listFragmentController.handleActivityResult(requestCode,resultCode,data);
        }
    }


