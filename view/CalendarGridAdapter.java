package com.example.luke.assignment1.view;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.luke.assignment1.R;
import com.example.luke.assignment1.model.Event;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by Luke on 16/08/2016.
 */
public class CalendarGridAdapter extends BaseAdapter {

    private Calendar previousMonthCalendar;
    private Calendar currentMonthCalendar;
    private SimpleDateFormat day = new SimpleDateFormat("dd");
    private SimpleDateFormat full = new SimpleDateFormat("dd/MM/yyyy");
    private SimpleDateFormat monthYear = new SimpleDateFormat("MM/yyyy");
    private int[] calendarDaysArray;
    int previousMonthEndPointOnCalendar;
    int currentMonthEndPoint;
    private ArrayList<Event> eventsArray;
    private Context context;

    public CalendarGridAdapter(Context context, ArrayList<Event> eventsArray, Calendar currentMonthCalendar) {
        this.context = context;
        this.previousMonthCalendar = Calendar.getInstance();
        this.currentMonthCalendar = currentMonthCalendar;
        this.calendarDaysArray = new int[42];
        this.eventsArray = eventsArray;

        generateCalendarArray(currentMonthCalendar);
    }
    @Override
    public int getCount() {
        return 42;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = LayoutInflater.from(context).inflate(R.layout.row_basic_grid, null);
        TextView icon = (TextView) view.findViewById(R.id.rowBasicGridTextView);
        icon.setText(String.valueOf(calendarDaysArray[i]));
        if(i <= previousMonthEndPointOnCalendar || i > currentMonthEndPoint + previousMonthEndPointOnCalendar){
            icon.setTextColor(Color.GRAY);
        } else {
            icon.setTextColor(Color.BLACK);
            for (Event event : eventsArray) {
                if(monthYear.format(event.getStartDate()).equals(monthYear.format(currentMonthCalendar.getTime())) &&
                        (day.format(event.getStartDate()).equals("0" + icon.getText()) || day.format(event.getStartDate()).equals(icon.getText()))) {
                            view.setBackgroundResource(R.drawable.rounded_background);
                            icon.setTextColor(Color.WHITE);
                }
            }
        }
        icon.setTextSize(20);

        return view;
    }

    public void generateCalendarArray(Calendar passedMonthCalendar) {

        this.currentMonthCalendar = passedMonthCalendar;
        previousMonthCalendar.set(Calendar.YEAR, currentMonthCalendar.get(currentMonthCalendar.YEAR));
        previousMonthCalendar.set(Calendar.MONTH, currentMonthCalendar.get(currentMonthCalendar.MONTH));
        previousMonthCalendar.set(Calendar.DAY_OF_MONTH, 1);
        previousMonthCalendar.add(Calendar.DAY_OF_MONTH, -1);

        currentMonthCalendar.set(Calendar.DAY_OF_MONTH, currentMonthCalendar.getActualMinimum(Calendar.DAY_OF_MONTH));

        int lastDayOfPreviousMonthsWeekday = previousMonthCalendar.get(Calendar.DAY_OF_WEEK);

        switch (lastDayOfPreviousMonthsWeekday) {
            case 1: previousMonthEndPointOnCalendar = 0;
                break;
            case 2: previousMonthEndPointOnCalendar = 1;
                break;
            case 3: previousMonthEndPointOnCalendar = 2;
                break;
            case 4: previousMonthEndPointOnCalendar = 3;
                break;
            case 5: previousMonthEndPointOnCalendar = 4;
                break;
            case 6: previousMonthEndPointOnCalendar = 5;
                break;
            case 7: previousMonthEndPointOnCalendar = 6;
                break;
            default: previousMonthEndPointOnCalendar = 0;
                break;
        }

        for (int i = 0; i <  lastDayOfPreviousMonthsWeekday; i++) {
            calendarDaysArray[i] = previousMonthCalendar.get(Calendar.DAY_OF_MONTH) - previousMonthEndPointOnCalendar + i;
        }

        int currentMonthIncrementer = 0;
        int followingMonthIncrementer = 0;
        currentMonthEndPoint = currentMonthCalendar.getActualMaximum(Calendar.DAY_OF_MONTH);

        for (int i = lastDayOfPreviousMonthsWeekday; i < calendarDaysArray.length ; i++) {

            if(currentMonthIncrementer >= currentMonthEndPoint) {
                followingMonthIncrementer++;
                calendarDaysArray[i] = followingMonthIncrementer;
            } else {
                calendarDaysArray[i] = currentMonthCalendar.get(Calendar.DAY_OF_MONTH) + currentMonthIncrementer;
                currentMonthIncrementer++;
            }
        }

    }
}
