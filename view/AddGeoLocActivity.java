package com.example.luke.assignment1.view;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Geocoder;
import android.net.ParseException;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.luke.assignment1.R;
import com.example.luke.assignment1.controller.AddContactActivityController;
import com.example.luke.assignment1.controller.AddEventActivityController;
import com.example.luke.assignment1.controller.AddGeoLocActivityController;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * Created by Luke on 20/09/2016.
 */
public class AddGeoLocActivity extends AppCompatActivity {

    private GoogleMap mMap;
    public static final int ERROR_DIALOG_REQUEST = 101;
    private static final double MELBOURNE_LAT = -37.782539, MELBOURNE_LNG = 144.966591;
    private static final float START_ZOOM = 12;
    private EditText mapSearch;
    private Button mapSearchButton;
    private Button mapDoneButton;
    private AddGeoLocActivityController addGeoLocActivityController;
    private Intent sentIntent;
    private final String delim = ",";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sentIntent = getIntent();
        if (servicesOK()) {
            setContentView(R.layout.activity_maps);
            initMap();
        }
        mapSearch = (EditText) findViewById(R.id.locationSearchEditText);
        mapSearchButton = (Button) findViewById(R.id.mapSearchButton);
        mapDoneButton = (Button) findViewById(R.id.mapDoneButton);
        addGeoLocActivityController = new AddGeoLocActivityController(this, mapSearch);
        mapDoneButton.setOnClickListener(addGeoLocActivityController);
        mapSearchButton.setOnClickListener(addGeoLocActivityController);
    }

//    @Override
//    protected void onResume(){
//        super.onResume();
////        setUpMapIfNeeded();
//    }

//    private void setUpMapIfNeeded() {
//        if (mMap == null) {
//
////            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
//
//            if (mMap != null) {
//                setUpMap();
//            }
//        }
//    }

//    private void setUpMap() {
//        mMap.addMarker(new MarkerOptions().position(new LatLng(0,0)).title("Marker"));
//    }

    public boolean servicesOK() {

        GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
        int isAvailable = googleApiAvailability.isGooglePlayServicesAvailable(this);

        if (isAvailable == ConnectionResult.SUCCESS) {
            return true;
        } else if (googleApiAvailability.isUserResolvableError(isAvailable)) {
            Dialog dialog = googleApiAvailability.getErrorDialog(this, isAvailable, ERROR_DIALOG_REQUEST);
            dialog.show();
        } else {
            Toast.makeText(this, "Cannot connect to mapping service", Toast.LENGTH_SHORT).show();
        }

        return false;
    }

    private void initMap() {
        if (mMap == null) {
            MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
            mapFragment.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap googleMap) {
                    mMap = googleMap;
                    LatLng location = new LatLng(MELBOURNE_LAT, MELBOURNE_LNG);
                    if(sentIntent.hasExtra(AddEventActivityController.PASSED_GEOLOC)){
                        String passedGeoLoc = sentIntent.getStringExtra(AddEventActivityController.PASSED_GEOLOC);
                        try{
                            String[] geoLocArray = passedGeoLoc.split(delim);
                            String latString = geoLocArray[0].trim();
                            String lngString = geoLocArray[1].trim();
                            Double lat = Double.parseDouble(latString);
                            Double lng = Double.parseDouble(lngString);
                            LatLng passedLocation = new LatLng(lat,lng);
                            location = passedLocation;
                            mapSearch.setText(passedGeoLoc);
                        } catch (NumberFormatException | ArrayIndexOutOfBoundsException e){
                            Log.d(MainActivity.DEBUG_TAG, e + " Caught.");
                        }
                    }
                        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(location, START_ZOOM);
                        mMap.moveCamera(cameraUpdate);

                    addGeoLocActivityController.setMap(mMap);
                }
            });
        }
    }
}
