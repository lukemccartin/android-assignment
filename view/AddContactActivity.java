package com.example.luke.assignment1.view;

import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.util.Pair;
import android.widget.Button;
import android.widget.ListView;
import com.example.luke.assignment1.R;
import com.example.luke.assignment1.controller.AddContactActivityController;
import com.example.luke.assignment1.controller.AddEventActivityController;

import java.util.ArrayList;


/**
 * Created by Luke on 11/08/2016.
 */
public class AddContactActivity extends AppCompatActivity { //implements AdapterView.OnItemClickListener {

    private ArrayList<String> importedContactsArray;
    private Cursor cursor;
    private Uri uri;
    private String[] projection;
    private ListView contactsList;
    private Button done;
    private ContactListAdapter adapter;
    private AddContactActivityController addContactActivityController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_contacts);

        importedContactsArray = new ArrayList<String>();
        contactsList = (ListView) findViewById(R.id.addContactListView);
        done = (Button) findViewById(R.id.addContactDoneButton);

        uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        projection = new String[]{ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME};
        cursor = getContentResolver().query(uri, projection, null, null, null);

        int indexName = cursor.getColumnIndex(projection[0]);
        cursor.moveToFirst();
        if(cursor.getCount() > 0) {
            do {
                String name = cursor.getString(indexName);
                importedContactsArray.add(name);
            } while (cursor.moveToNext());
        }
        cursor.close();

        ArrayList<String> passedContactsArray = getIntent().getStringArrayListExtra(AddEventActivityController.PASSED_ARRAY);
        ArrayList<Pair> pairedArray = new ArrayList<Pair>();

        if(!(passedContactsArray.isEmpty())){
            for (int i = 0; i < importedContactsArray.size(); i++) {
                Pair pair;
                if(passedContactsArray.contains(importedContactsArray.get(i))){
                    pair = new Pair(1,importedContactsArray.get(i));
                } else {
                    pair = new Pair(0,importedContactsArray.get(i));
                }
                pairedArray.add(pair);
            }
        } else {
            for (int i = 0; i < importedContactsArray.size(); i++) {
                Pair pair = new Pair(0,importedContactsArray.get(i));
                pairedArray.add(pair);
            }
        }

        adapter = new ContactListAdapter(AddContactActivity.this, pairedArray);
        contactsList.setAdapter(adapter);
        addContactActivityController = new AddContactActivityController(this, importedContactsArray, contactsList);
        contactsList.setOnItemClickListener(addContactActivityController);
        done.setOnClickListener(addContactActivityController);
    }
}
