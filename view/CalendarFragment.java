package com.example.luke.assignment1.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;

import com.example.luke.assignment1.R;
import com.example.luke.assignment1.controller.CalendarFragmentController;
import com.example.luke.assignment1.model.Event;
import com.example.luke.assignment1.model.Singleton;
import com.example.luke.assignment1.view.CalendarGridAdapter;
import com.example.luke.assignment1.view.MainActivity;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by Luke on 7/08/2016.
 */
public class CalendarFragment extends Fragment {

    private View rootView;
    private TextView monthTitle;
    private Button prevButton;
    private Button nextButton;
    private GridView datesGrid;
    private Calendar currentMonthCalendar;
    private final DateFormat titleDateFormat = new SimpleDateFormat("MMMM yyy");
    private CalendarGridAdapter adapter;
    private CalendarFragmentController calendarFragmentController;

    public CalendarFragment(){

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_calendar, container, false);
        monthTitle = (TextView)rootView.findViewById(R.id.monthTitle);
        prevButton = (Button)rootView.findViewById(R.id.previousButton);
        nextButton = (Button)rootView.findViewById(R.id.nextButton);
        datesGrid = (GridView)rootView.findViewById(R.id.gridViewDates);

        currentMonthCalendar = Calendar.getInstance();
        MainActivity mainActivity = (MainActivity) getActivity();
        calendarFragmentController = mainActivity.getCalendarFragmentController();

        datesGrid.setOnItemClickListener(calendarFragmentController);
        datesGrid.setOnItemLongClickListener(calendarFragmentController);
        prevButton.setOnClickListener(calendarFragmentController);
        nextButton.setOnClickListener(calendarFragmentController);

        adapter = new CalendarGridAdapter(getActivity().getApplicationContext(), calendarFragmentController.getEventsArray(), currentMonthCalendar);
        monthTitle.setText(titleDateFormat.format(currentMonthCalendar.getTime()));
        datesGrid.setAdapter(adapter);

        return rootView;
    }

    public void setMonthTitle(String monthTitle){
        this.monthTitle.setText(monthTitle);
    }

    public String getMonthTitle() {
        String monthTitleString = monthTitle.getText().toString();
        return monthTitleString;
    }

    public CalendarGridAdapter getAdapter(){
        return adapter;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        calendarFragmentController.handleActivityResult(requestCode,resultCode,data);
    }
}
