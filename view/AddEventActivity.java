package com.example.luke.assignment1.view;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.example.luke.assignment1.R;
import com.example.luke.assignment1.controller.AddEventActivityController;
import com.example.luke.assignment1.model.Event;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class AddEventActivity extends AppCompatActivity {

    private EditText title;
    private EditText startDate;
    private EditText startTime;
    private EditText endDate;
    private EditText endTime;
    private EditText venue;
    private EditText notes;
    private EditText geoLoc;
    private Button mapButton;
    private Button addButton;
    private ListView selectedContacts;
    private Button doneButton;
    private ArrayList<String> selectedContactsArray;
    private BasicListAdapter adapter;
    private SimpleDateFormat sdfDate = new SimpleDateFormat("dd/MM/yyyy");
    private SimpleDateFormat sdfTime = new SimpleDateFormat("HH:mm");
    private ArrayList<Event> passedInEventsArray;
    private AddEventActivityController addEventActivityController;
    private Intent sentIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_event);
        sentIntent = getIntent();

        title = (EditText) findViewById(R.id.titleEditText);
        venue = (EditText) findViewById(R.id.venueEditText);
        geoLoc= (EditText) findViewById(R.id.geoLocationEditText);
        notes = (EditText) findViewById(R.id.noteEditText);
        startDate = (EditText) findViewById(R.id.startDateEditText);
        startTime = (EditText) findViewById(R.id.startTimeEditText);
        endDate = (EditText) findViewById(R.id.endDateEditText);
        endTime = (EditText) findViewById(R.id.endTimeEditText);
        selectedContacts = (ListView) findViewById(R.id.chosenContactsListView);

        if(sentIntent.hasExtra(MainActivity.PASSED_EVENT)) {

            Event passedEvent = getIntent().getParcelableExtra(MainActivity.PASSED_EVENT);
            passedInEventsArray = getIntent().getParcelableArrayListExtra(MainActivity.EVENTS_ARRAY);
            title.setText(passedEvent.getTitle());
            venue.setText(passedEvent.getVenue());
            geoLoc.setText(passedEvent.getGeoLocation());
            if(passedEvent.getNote() != null){
                notes.setText(passedEvent.getNote());
            }
            String eventStartDate = sdfDate.format(passedEvent.getStartDate());
            startDate.setText(eventStartDate);
            String eventStartTime = sdfTime.format(passedEvent.getStartDate());
            startTime.setText(eventStartTime);
            String eventEndDate = sdfDate.format(passedEvent.getEndDate());
            endDate.setText(eventEndDate);
            String eventEndTime = sdfTime.format(passedEvent.getEndDate());
            endTime.setText(eventEndTime);

            selectedContactsArray = passedEvent.getAttendees();
            adapter = new BasicListAdapter(AddEventActivity.this, selectedContactsArray);
            selectedContacts.setAdapter(adapter);

        } else {
            passedInEventsArray = getIntent().getParcelableArrayListExtra(MainActivity.EVENTS_ARRAY);
            selectedContactsArray = new ArrayList<String>();
            adapter = new BasicListAdapter(AddEventActivity.this, selectedContactsArray);
            selectedContacts.setAdapter(adapter);
        }

        addEventActivityController = new AddEventActivityController(this, sentIntent,
                selectedContactsArray, passedInEventsArray, title, startDate, startTime, endDate,
                endTime, venue, notes, geoLoc);

        addButton = (Button) findViewById(R.id.addAttendeesButton);
        addButton.setOnClickListener(addEventActivityController);

        mapButton = (Button) findViewById(R.id.mapButton);
        mapButton.setOnClickListener(addEventActivityController);

        doneButton = (Button) findViewById(R.id.doneButton);
        doneButton.setOnClickListener(addEventActivityController);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        addEventActivityController.handleActivityResult(requestCode, resultCode, data);
        adapter.notifyDataSetChanged();
    }
}

