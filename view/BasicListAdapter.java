package com.example.luke.assignment1.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.example.luke.assignment1.R;
import java.util.ArrayList;


/**
 * Created by Luke on 12/08/2016.
 */
public class BasicListAdapter extends ArrayAdapter<String> {

    public BasicListAdapter(Context context, ArrayList<String> contacts) {
        super(context,0, contacts);
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {

        String contact = getItem(position);
        if(view == null){
            view = LayoutInflater.from(getContext()).inflate(R.layout.row_basic, parent, false);
        }

        TextView contactName = (TextView) view.findViewById(R.id.rowBasicTextView);
        contactName.setText(contact);

        return view;
    }
}
